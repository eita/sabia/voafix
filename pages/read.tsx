import React from "react";
import { GetServerSidePropsContext, GetStaticPaths } from "next";

import ItemPageHead from "@/components/ItemPageHead";
import ItemPageBack from "@/components/ItemPageBack";
import { getItem } from "@/libs/getItem";
import { getActionGerundio } from "@/libs/getActionGerundio";
import { getAllSlugsAndIds } from "@/libs/getAllSlugsAndIds";
import { ActionType, ItemInterface } from "@/types";

interface PageProps {
  data: ItemInterface | false;
}

const action: ActionType = "leia";

const Read = ({ data }: PageProps) => {
  if (!data) {
    return <></>;
  }

  // const url = `https://docs.google.com/gview?url=${data.wpdmpro_file_url}&embedded=true`;
  const url = data.wpdmpro_file_url;

  return (
    <>
      <ItemPageHead data={data} action={"leia"} slug={data.post_name} />
      <div className="h-screen w-screen bg-black">
        <nav className="fixed w-full p-4 z-10 flex flex-row items-center gap-3 md:gap-8 bg-zinc-800 bg-opacity-70">
          <ItemPageBack />
          <p className="text-white text-1xl md:text-3xl font-bold line-clamp-2">
            <span className="font-light">{getActionGerundio(action)}:</span>{" "}
            {data.post_title}
          </p>
        </nav>
        {data.wpdmpro_file_url ? (
          <iframe src={url} className="h-full w-full pt-20"></iframe>
        ) : (
          <div className="h-full w-full items-center">
            Erro ao tentar abrir o documento!
          </div>
        )}
      </div>
    </>
  );
};

export const getStaticProps = async (context: GetServerSidePropsContext) => {
  const { id } = context.params || {};
  if (!id || Array.isArray(id) || !parseInt(id)) {
    return {
      notFound: true,
    };
  }
  const data = await getItem({ id: parseInt(id) });
  return {
    props: {
      data,
    },
    revalidate: 60,
  };
};

export default Read;
