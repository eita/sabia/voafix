import React from "react";
import { useSearchParams } from "next/navigation";

import PageWithFiltersContent from "@/components/PageWithFiltersContent";
import { ItemType } from "@/types";

const Decada = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("id");
  const postType = searchParams.get("post_type");

  return (
    <PageWithFiltersContent
      title={`Década de {TERM}`}
      taxonomy="decada"
      termId={id ? parseInt(id) : undefined}
      emptyMessage={
        postType
          ? "Ooops. Não há {POST_TYPE} na década de {TERM}."
          : "Ooops. Não há nenhuma mídia na década de {TERM}."
      }
      postType={postType ? (postType as ItemType) : undefined}
    />
  );
};

export default Decada;
