import React from "react";
import { useSearchParams } from "next/navigation";

import PageWithFiltersContent from "@/components/PageWithFiltersContent";
import { ItemType } from "@/types";

const Tema = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("id");
  const postType = searchParams.get("post_type");

  return (
    <PageWithFiltersContent
      title={`Tema: {TERM}`}
      taxonomy="tema"
      termId={id ? parseInt(id) : undefined}
      emptyMessage={
        postType
          ? 'Ooops. Não há {POST_TYPE} no tema "{TERM}".'
          : 'Ooops. Não há nenhuma mídia no tema "{TERM}".'
      }
      postType={postType ? (postType as ItemType) : undefined}
    />
  );
};

export default Tema;
