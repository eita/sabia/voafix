import React from "react";
import { GetServerSidePropsContext, GetStaticPaths } from "next";

import ItemPageHead from "@/components/ItemPageHead";
import ItemPageBack from "@/components/ItemPageBack";
import { getItem } from "@/libs/getItem";
import { getActionGerundio } from "@/libs/getActionGerundio";
import { getAllSlugsAndIds } from "@/libs/getAllSlugsAndIds";
import { ActionType, ItemInterface } from "@/types";

interface PageProps {
  data: ItemInterface | false;
}

const action: ActionType = "assista";

const Watch = ({ data }: PageProps) => {
  if (!data) {
    return <></>;
  }

  return (
    <>
      <ItemPageHead data={data} action={"assista"} slug={data.post_name} />
      <div className="h-screen w-screen bg-black">
        <nav className="fixed w-full p-4 z-10 flex flex-row items-center gap-3 md:gap-8 bg-zinc-800 bg-opacity-70">
          <ItemPageBack />
          <p className="text-white text-1xl md:text-3xl font-bold line-clamp-2">
            <span className="font-light">{getActionGerundio(action)}:</span>{" "}
            {data.post_title}
          </p>
        </nav>
        {/* <video className="h-full w-full" autoPlay controls src={data?.videoUrl}></video> */}
        <iframe
          className="h-full w-full"
          src={`https://www.youtube.com/embed/${data.external_id}?autoplay=1&rel=0`}
          title={data.post_title}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      </div>
    </>
  );
};

export const getStaticProps = async (context: GetServerSidePropsContext) => {
  const { id } = context.params || {};
  if (!id || Array.isArray(id) || !parseInt(id)) {
    return {
      notFound: true,
    };
  }
  const data = await getItem({ id: parseInt(id) });
  return {
    props: {
      data,
    },
    revalidate: 60,
  };
};

export default Watch;
