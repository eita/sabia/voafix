import React from "react";
import { GetServerSidePropsContext, GetStaticPaths } from "next";

import ItemPageHead from "@/components/ItemPageHead";
import ItemPageBack from "@/components/ItemPageBack";
import { getItem } from "@/libs/getItem";
import { getActionGerundio } from "@/libs/getActionGerundio";
import { getAllSlugsAndIds } from "@/libs/getAllSlugsAndIds";
import { ItemInterface, ItemType } from "@/types";
import { getActionFromItemType } from "@/libs/getActionFromItemType";

interface PageProps {
  data: ItemInterface | false;
  slug: string;
}

const itemType: ItemType = "video";
const action = getActionFromItemType(itemType);

const Watch = ({ data, slug }: PageProps) => {
  if (!data) {
    return <></>;
  }

  return (
    <>
      <ItemPageHead data={data} action={action} slug={slug} />
      <div className="h-screen w-screen bg-black">
        <nav className="fixed w-full p-4 z-10 flex flex-row items-center gap-3 md:gap-8 bg-zinc-800 bg-opacity-70">
          <ItemPageBack />
          <p className="text-white text-1xl md:text-3xl font-bold line-clamp-2">
            <span className="font-light">{getActionGerundio(action)}:</span>{" "}
            {data.post_title}
          </p>
        </nav>
        {/* <video className="h-full w-full" autoPlay controls src={data?.videoUrl}></video> */}
        <iframe
          className="h-full w-full"
          src={`https://www.youtube.com/embed/${data.external_id}?autoplay=1&rel=0`}
          title={data.post_title}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      </div>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const slugs = await getAllSlugsAndIds("slugs", itemType);
  if (!slugs) {
    return {
      paths: ["none"].map((slug) => ({
        params: { slug },
      })),
      fallback: false,
    };
  }

  return {
    paths: slugs.map((slug) => ({
      params: { slug },
    })),
    fallback: "blocking", // This allows Next.js to generate pages on demand if they weren’t pre-rendered
  };
};

export const getStaticProps = async (context: GetServerSidePropsContext) => {
  const { slug } = context.params || {};
  if (!slug || Array.isArray(slug)) {
    return {
      notFound: true,
    };
  }
  const data = await getItem({ action, slug });
  return {
    props: {
      data,
      slug,
    },
    revalidate: 24 * 3600,
  };
};

export default Watch;
