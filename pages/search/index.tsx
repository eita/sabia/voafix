import React from "react";
import { useSearchParams } from "next/navigation";

import PageWithFiltersContent from "@/components/PageWithFiltersContent";
import { ItemType } from "@/types";

const Search = () => {
  const searchParams = useSearchParams();
  const s = searchParams.get("s");
  const postType = searchParams.get("post_type");

  return (
    <PageWithFiltersContent
      title={`Busca: "{TERM}"`}
      taxonomy="search"
      termSlug={s || undefined}
      emptyMessage={
        postType
          ? 'Ooops. Não há {POST_TYPE} para a busca "{TERM}".'
          : 'Ooops. Não há resultados para a busca "{TERM}".'
      }
      postType={postType ? (postType as ItemType) : undefined}
    />
  );
};

export default Search;
