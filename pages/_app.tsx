import React, { useEffect, useState } from "react";
import type { AppProps } from "next/app";
import Head from "next/head";

import "../styles/globals.css";
import Layout from "@/components/Layout";
import useNavigationHistory from "@/hooks/useNavigationHistory";
import { useRouter } from "next/router";
import "../styles/globals.css";
import SubscriptionModal from "@/components/SubscriptionModal";
import useSubscriptionModalStore from "@/hooks/useSubscriptionModalStore";
import { User } from "@/types";

declare global {
  interface Window {
    _mtm: any;
  }
}

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { handleBackNavigation } = useNavigationHistory();

  useEffect(() => {
    const localHandleBackNavigation = () => {
      history.pushState(null, "", router.asPath);
      handleBackNavigation();
    };

    window.addEventListener("popstate", localHandleBackNavigation);

    // Cleanup listener on component unmount
    return () => {
      window.removeEventListener("popstate", localHandleBackNavigation);
    };
  }, [router, handleBackNavigation]);
  const { isOpen, openModal, closeModal } = useSubscriptionModalStore();
  const [user, setUser] = useState<User | null>(null);

  React.useEffect(() => {
    document.documentElement.lang = "pt-BR";
  }, [])

  const launchSubscriptionModal = () => {
    setTimeout(() => {
      localStorage.setItem(
        "voaflixState",
        JSON.stringify({
          latestAttempt: new Date().toISOString(),
        })
      );
      openModal();
    }, 40000);
  };

  const iframeListener = (
    e: MessageEvent<{
      fields: Record<number, { id: number; key: string; value: string }>;
    }>
  ) => {
    if (!e || !e.data || !e.data.fields) {
      return;
    }
    const fields = e.data.fields;
    const newUser: User = {
      name: "",
    };
    for (const field of Object.values(fields)) {
      const key = field.key.split("_")[0];
      switch (key) {
        case "email":
          newUser.email = field.value;
          break;
        case "nome":
          newUser.name = field.value;
          break;
        case "area":
          newUser.segment = field.value;
          break;
        case "city":
          newUser.city = field.value;
          break;
      }
    }
    if (newUser.name !== "") {
      setUser(newUser);
      localStorage.setItem("voaflixState", JSON.stringify({ user: newUser }));
    }
    setTimeout(() => {
      closeModal();
    }, 3000);
  };

  useEffect(() => {
    window.addEventListener("message", iframeListener);
    return () => window.removeEventListener("message", iframeListener);
  }, []);

  useEffect(() => {
    const voaflixStateRaw = localStorage.getItem("voaflixState");
    if (!voaflixStateRaw) {
      launchSubscriptionModal();
      return;
    }
    const voaflixState = JSON.parse(voaflixStateRaw);
    if (!voaflixState || !voaflixState.user) {
      launchSubscriptionModal();
      return;
    }
    setUser(voaflixState.user);
  }, []);

  return (
    <Layout>    
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1"
        />
      </Head>
      <SubscriptionModal visible={isOpen} handleClose={closeModal} />
      <Component {...pageProps} />
    </Layout>
  );
}
