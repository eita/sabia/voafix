import React from "react";
import { GetServerSidePropsContext, GetStaticPaths } from "next";

import ItemPageHead from "@/components/ItemPageHead";
import ItemPageBack from "@/components/ItemPageBack";
import { getItem } from "@/libs/getItem";
import { getActionGerundio } from "@/libs/getActionGerundio";
import { getAllSlugsAndIds } from "@/libs/getAllSlugsAndIds";
import { ActionType, ItemInterface } from "@/types";

interface PageProps {
  data: ItemInterface | false;
}

const action: ActionType = "escute";

const Listen = ({ data }: PageProps) => {
  if (!data) {
    return <></>;
  }

  return (
    <>
      <ItemPageHead data={data} action={"escute"} slug={data.post_name} />
      <div className="h-screen w-screen bg-black">
        <nav className="fixed w-full p-4 z-10 flex flex-row items-center gap-3 md:gap-8 bg-zinc-800 bg-opacity-70">
          <ItemPageBack />
          <p className="text-white text-1xl md:text-3xl font-bold line-clamp-2">
            <span className="font-light">{getActionGerundio(action)}:</span>{" "}
            {data.post_title}
          </p>
        </nav>
        {/* <video className="h-full w-full" autoPlay controls src={data?.videoUrl}></video> */}
        <iframe
          src={`https://open.spotify.com/embed/episode/${data.external_id}`}
          className="h-full w-full"
          allowFullScreen
          allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
          loading="lazy"
          title={data.post_title}
        ></iframe>
      </div>
    </>
  );
};

export const getStaticProps = async (context: GetServerSidePropsContext) => {
  const { id } = context.params || {};
  if (!id || Array.isArray(id) || !parseInt(id)) {
    return {
      notFound: true,
    };
  }
  const data = await getItem({ id: parseInt(id) });
  return {
    props: {
      data,
    },
    revalidate: 60,
  };
};

export default Listen;
