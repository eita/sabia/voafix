import React from "react";
import { useSearchParams } from "next/navigation";

import PageWithFiltersContent from "@/components/PageWithFiltersContent";
import { ItemType } from "@/types";

const Wpdmcategory = () => {
    const searchParams = useSearchParams();
    const id = searchParams.get("id");
    const postType = searchParams.get("post_type");

    return (
        <PageWithFiltersContent
            title={`{TERM}`}
            taxonomy="wpdmcategory"
            termId={id ? parseInt(id) : undefined}
            emptyMessage={
                postType
                    ? "Ooops. Não há {POST_TYPE} do tipo {TERM}."
                    : "Ooops. Não há nenhuma mídia do tipo {TERM}."
            }
            postType={postType ? (postType as ItemType) : undefined}
        />
    );
};

export default Wpdmcategory;
