import React from "react";
import { useSearchParams } from "next/navigation";

import PageWithFiltersContent from "@/components/PageWithFiltersContent";

const PostType = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("id");

  return (
    <PageWithFiltersContent
      title={`Mídia: {TERM}`}
      taxonomy="postType"
      termSlug={id || undefined}
      emptyMessage={`Ooops. Não há nenhuma mídia.`}
    />
  );
};

export default PostType;
