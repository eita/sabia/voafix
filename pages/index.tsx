import React, { useState } from "react";

import Navbar from "@/components/Navbar";
import Billboard from "@/components/Billboard";
import ItemListSlider from "@/components/ItemListSlider";

import Footer from "@/components/Footer";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import { RowsConfig, User } from "@/types";
import useTaxonomyList from "@/hooks/useTaxonomyList";
import Head from "next/head";
import InfoModal from "@/components/InfoModal";


const Home = () => {

  const { isOpen, closeModal } = useInfoModalStore();
  const [user, setUser] = useState<User | null>(null);
  const { data: temas } = useTaxonomyList("tema", true);
  const { data: decadas } = useTaxonomyList("decada", true);

  const rows: RowsConfig[] = [];

  rows.push({
    taxonomy: "tema",
    term: "destaques",
    title: "Destaques",
  });
  if (temas && temas.length > 0) {
    for (let i = 0; i < 3; i++) {
      rows.push({
        taxonomy: "tema",
        term: temas[i].slug,
        title: temas[i].name,
      });
    }
  }
  if (decadas && decadas.length > 0) {
    for (let i = 0; i < 2; i++) {
      rows.push({
        taxonomy: "decada",
        term: decadas[i].slug,
        title: `Década de ${decadas[i].name}`,
      });
    }
  }
  rows.push({
    taxonomy: "postType",
    term: "video",
    title: "Assista",
  });
  rows.push({
    taxonomy: "postType",
    term: "audio",
    title: "Escute",
  });
  rows.push({
    taxonomy: "postType",
    term: "wpdmpro",
    title: "Leia",
  });

  return (
    <>
      <Head>
        <title>VoaFlix</title>
        <meta name="description" content="Biblioteca do Centro Sabiá" />
        <meta property="og:title" content="VoaFlix" />
        <meta property="og:description" content="Biblioteca do Centro Sabiá" />
        <meta property="og:image" content="https://voaflix.centrosabia.org.br/images/logo.png" />
        <meta
          property="og:url"
          content="https://voaflix.centrosabia.org.br"
        />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Voaflix" />
      </Head>
      <InfoModal visible={isOpen} onClose={closeModal} />
      <Navbar user={user} />
      <Billboard />
      <div>
        {rows.map((row) => (
          <ItemListSlider
            key={`${row.taxonomy}_${row.term}`}
            title={row.title}
            taxonomy={row.taxonomy}
            term={row.term}
          />
        ))}
      </div>
      <Footer />
    </>
  );
};

export default Home;
