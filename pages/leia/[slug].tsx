import React from "react";
import { GetServerSidePropsContext, GetStaticPaths } from "next";

import ItemPageHead from "@/components/ItemPageHead";
import ItemPageBack from "@/components/ItemPageBack";
import { getItem } from "@/libs/getItem";
import { getActionGerundio } from "@/libs/getActionGerundio";
import { getAllSlugsAndIds } from "@/libs/getAllSlugsAndIds";
import { ItemInterface, ItemType } from "@/types";
import { getActionFromItemType } from "@/libs/getActionFromItemType";

interface PageProps {
  data: ItemInterface | false;
  slug: string;
}

const itemType: ItemType = "wpdmpro";
const action = getActionFromItemType(itemType);

const Read = ({ data, slug }: PageProps) => {
  if (!data) {
    return <></>;
  }

  // const url = `https://docs.;le.com/gview?url=${data.wpdmpro_file_url}&embedded=true`;
  const url = data.wpdmpro_file_url;

  return (
    <>
      <ItemPageHead data={data} action={action} slug={slug} />
      <div className="h-screen w-screen bg-black">
        <nav className="fixed w-full p-4 z-10 flex flex-row items-center gap-3 md:gap-8 bg-zinc-800 bg-opacity-70">
          <ItemPageBack />
          <p className="text-white text-1xl md:text-3xl font-bold line-clamp-2">
            <span className="font-light">{getActionGerundio(action)}:</span>{" "}
            {data.post_title}
          </p>
        </nav>
        {data.wpdmpro_file_url ? (
          <iframe src={url} className="h-full w-full pt-20"></iframe>
        ) : (
          <div className="h-full w-full items-center">
            Erro ao tentar abrir o documento!
          </div>
        )}
      </div>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const slugs = await getAllSlugsAndIds("slugs", itemType);
  if (!slugs) {
    return {
      paths: ["none"].map((slug) => ({
        params: { slug },
      })),
      fallback: false,
    };
  }

  return {
    paths: slugs.map((slug) => ({
      params: { slug },
    })),
    fallback: "blocking", // This allows Next.js to generate pages on demand if they weren’t pre-rendered
  };
};

export const getStaticProps = async (context: GetServerSidePropsContext) => {
  const { slug } = context.params || {};
  if (!slug || Array.isArray(slug)) {
    return {
      notFound: true,
    };
  }
  const data = await getItem({ action, slug });
  return {
    props: {
      data,
      slug,
    },
    revalidate: 24 * 3600,
  };
};

export default Read;
