/** @type {import('tailwindcss').Config} */

// import plugin from "tailwindcss";
const plugin = require("tailwindcss/plugin");

module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        red: {
          50: "#FDEAE7",
          75: "#F8AA9B",
          100: "#F68771",
          200: "#F25334",
          300: "#EF300A",
          400: "#A72207",
          500: "#921D06",
        },
        green: {
          50: "#E7F3EE",
          75: "#9BCDBB",
          100: "#72B89F",
          200: "#359975",
          300: "#0C8459",
          400: "#085C3E",
          500: "#075136",
        },
        white: {
          DEFAULT: "#FFFFFF",
          50: "#FFFDFB",
          75: "#FDF6F0",
          100: "#FDF3EA",
          200: "#FCEEE0",
          300: "#FBEADA",
          400: "#B0A499",
          500: "#998F85",
        },
      },
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      addUtilities({
        ".no-scrollbar::-webkit-scrollbar": {
          display: "none",
        },
        ".no-scrollbar": {
          "-msOverflowStyle": "none",
          scrollbarWidth: "none",
        },
      });
    }),
  ],
};
