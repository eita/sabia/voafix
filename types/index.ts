// export interface MovieInterface {
//   id: string;
//   title: string;
//   description: string;
//   thumbnailUrl: string;
//   videoUrl: string;
//   duration: string;
//   genre: string;
// }

export type ItemType = 'video' | 'audio' | 'wpdmpro';
export type ActionType = 'assista' | 'escute' | 'leia';
export type TaxonomyType = 'tema' | 'wpdmcategory' | 'decada';
export type TaxonomyTypeExtended = TaxonomyType | 'postType' | 'search';
export type MimeType = string;
export interface Taxonomy {
  term_id?: number;
  name: string;
  slug: string;
}
export interface Term {
  term_id: number;
  name: string;
  slug: string;
  term_group: number;
  term_taxonomy_id: number;
  taxonomy: string;
  description: string;
  parent: number;
  count: number;
  filter: string;
  term_order: string;
}

export interface ItemInterface {
  ID: number;
  post_author: string;
  post_date: string;
  post_date_gmt: string;
  post_content: string;
  post_title: string;
  post_excerpt: string;
  post_status: 'publish' | 'private';
  comment_status: 'closed' | 'open';
  ping_status: 'closed' | 'open';
  post_password: string;
  post_name: string;
  to_ping: string;
  pinged: string;
  post_modified: string;
  post_modified_gmt: string;
  post_content_filtered: string;
  post_parent: number;
  guid: string;
  menu_order: number
  post_type: ItemType;
  post_mime_type: MimeType;
  comment_count: string;
  filter: string;
  cover_image_full: string;
  cover_image_1536: string;
  cover_image_large: string;
  cover_image_medium: string;
  featured_image_full: string;
  featured_image_medium: string;
  featured_image_thumb: string;
  external_id?: string;
  tax_tema: Taxonomy[];
  tax_wpdmcategory: Taxonomy[];
  tax_decada: Taxonomy[];
  wpdmpro_file_url?: string;
}

export type GenericValue = any;

export type Item = Record<string, GenericValue>;

export interface RowsConfig {
  taxonomy?: TaxonomyTypeExtended | 'tema';
  title: string | null;
  term?: string | null;
  data?: ItemInterface[];
  search?: string;
}

export interface serverEndPointType {
  url: string;
  apiTokenUrl: string;
  api?: string;
  signUp?: string;
  autocomplete?: string;
  getToken?: string;
  refreshToken?: string;
  verifyToken?: string;
}

export interface NavbarTerm {
  taxonomy: TaxonomyTypeExtended;
  termSlug: string;
  termName: string;
  termId?: number;
}

export interface User {
  name: string;
  email?: string;
  segment?: string;
  city?: string;
}