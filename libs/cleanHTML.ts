export const cleanHTML = (htmlText: string) => {
    return htmlText.replace(/<!--.*?-->|<[^>]+>/g, '').trim();
}