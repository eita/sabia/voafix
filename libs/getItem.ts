import { ActionType, ItemInterface, ItemType } from "@/types";
import axios from "axios";

interface GetItemProps {
  slug?: string;
  id?: number;
  action?: ActionType;
}

export const getItem = async ({id, slug, action}: GetItemProps) => {
    let url = process.env.NEXT_PUBLIC_WP_API_URL;
    if (!url) {
      return false;
    }
    if (id) {
      url += `item/${id}`;
    } else if (slug && action) {
      url += `${action}/${slug}`;
    }

    try {
      const res = await axios.get<ItemInterface>(url);
      if (res.data) {
        return res.data
      }
      console.log(`Error retrieving data from url ${url}`)
      console.log('Result: ', {status: res.status})
    } catch(e) {
      console.log(`Error retrieving data from url ${url}`)
      console.log('Error: ', e);
    }
    return false;
  }