import { ItemType } from "@/types";
import axios from "axios";

export const getAllSlugsAndIds = async (slugsOrIds: 'slugs' | 'ids', itemType: ItemType) => {
    const url = `${process.env.NEXT_PUBLIC_WP_API_URL}slugs_ids?post_type=${itemType}`;
    try {
      const res = await axios.get<string[][]>(url);
      if (res.data) {
        return res.data.map(row => slugsOrIds === 'ids' ? row[0] : row[1])
      }
    } catch(e) {
      console.log(e);
    }
    return false;
  }