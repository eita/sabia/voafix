export const getKey = (pageIndex:number, previousPageData:any, url:string | null) => {
    if (!url || (previousPageData && !previousPageData.length)) return null
    return `${url}&paged=${
      pageIndex + 1
    }`
  }