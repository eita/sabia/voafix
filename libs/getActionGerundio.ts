import { ActionType} from "@/types"

export const getActionGerundio = (action: ActionType) => {
    let gerundio:string;
      switch (action) {
        case 'escute':
          gerundio = 'Ouvindo';
          break;
        case 'assista':
          gerundio = 'Assistindo';
          break;
        case 'leia':
          gerundio = 'Lendo';
          break;
      }
    return gerundio
  }