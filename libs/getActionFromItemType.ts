import { ActionType, ItemType } from "@/types"

export const getActionFromItemType = (itemType: ItemType) => {
    let action:ActionType;
      switch (itemType) {
        case 'audio':
          action = 'escute';
          break;
        case 'video':
          action = 'assista';
          break;
        case 'wpdmpro':
          action = 'leia';
          break;
      }
    return action
  }