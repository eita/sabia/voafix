import { ItemType } from "@/types";

export const getPostTypeName = (postType: ItemType | null) => {
    if (!postType) {
        return null;
    }
    switch (postType) {
        case 'audio':
            return 'Podcast';
        case 'video':
            return 'Vídeo';
        case 'wpdmpro':
            return 'Documento PDF';
    }
}