import {
  MusicalNoteIcon,
  PlayIcon,
  BookOpenIcon,
} from "@heroicons/react/24/solid";
import { useRouter } from "next/router";
import { ItemType } from "@/types";

interface PlayButtonProps {
  itemSlug: string;
  mediaType: ItemType;
  isBillboard?: boolean;
  onPress?: () => void;
  noLabel?: boolean;
}

const PlayButton: React.FC<PlayButtonProps> = ({
  itemSlug,
  mediaType,
  isBillboard,
  onPress,
  noLabel,
}) => {
  const router = useRouter();

  let path: string;
  let action: string;
  let Icon: typeof PlayIcon;

  switch (mediaType) {
    case "audio":
      path = "escute";
      action = "Escutar";
      Icon = MusicalNoteIcon;
      break;
    case "video":
      path = "assista";
      action = "Assistir";
      Icon = PlayIcon;
      break;
    case "wpdmpro":
      path = "leia";
      action = "Ler";
      Icon = BookOpenIcon;
      break;
  }

  return (
    <button
      title={action}
      onClick={() => {
        if (onPress) {
          onPress();
        }
        router.push(`/${path}/${itemSlug}`);
      }}
      className={`
        bg-white 
        ${isBillboard ? "rounded-md" : ""}
        py-1 ${isBillboard ? "md:py-2" : ""}
        px-2 ${isBillboard ? "md:px-4" : ""}
        w-auto 
        text-xs ${isBillboard ? "lg:text-lg" : "lg:text-md"}
        ${isBillboard ? "" : "border border-red-300 text-red-300"}
        font-semibold
        flex
        flex-row
        items-center
        hover:bg-neutral-300
        transition
        `}
    >
      {Icon && (
        <Icon
          className={`w-4 md:w-7 ${
            isBillboard ? "text-black" : "text-red-300"
          } mr-1`}
        />
      )}
      {noLabel ? "" : action}
    </button>
  );
};

export default PlayButton;
