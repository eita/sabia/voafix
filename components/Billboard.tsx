import React, { useCallback } from "react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";

import PlayButton from "@/components/PlayButton";
import useBillboard from "@/hooks/useBillboard";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import DownloadButton from "./DownloadButton";
import { ItemType, TaxonomyType, TaxonomyTypeExtended } from "@/types";

interface BillboardProps {
  taxonomy?: TaxonomyTypeExtended;
  termSlug?: string;
  postType?: ItemType;
}

const Billboard: React.FC<BillboardProps> = ({
  taxonomy,
  termSlug,
  postType,
}) => {
  const { openModal } = useInfoModalStore();
  const { data } = useBillboard({ taxonomy, termSlug, postType });

  const handleOpenModal = useCallback(() => {
    openModal(data.ID);
  }, [openModal, data?.ID]);

  if (!data) {
    return <div className="relative h-[56.25vw] bg-zinc-800"></div>;
  }

  return (
    <div className="relative h-[56.25vw] bg-zinc-800">
      {data.post_type === "video" && data.external_id && (
        <iframe
          className="w-full h-[56.25vw] object-cover brightness-[60%] transition duration-500 gradient-to-b from-opacity-100 to-opacity-0"
          src={`https://www.youtube.com/embed/${data.external_id}?autoplay=1&fs=0&rel=0&mute=1`}
          title={data.post_title}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      )}
      {data.post_type === "audio" && data.external_id && (
        <div
          style={{
            backgroundImage: `url(${data.cover_image_large})`,
            backgroundSize: "cover",
          }}
        >
          <iframe
            className="pt-[100px] w-9/12 m-auto h-[56.25vw] object-cover brightness-[60%] transition duration-500"
            src={`https://open.spotify.com/embed/episode/${data.external_id}`}
            allowFullScreen
            allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
            loading="lazy"
            title={data.post_title}
          ></iframe>
        </div>
      )}
      {data.post_type === "wpdmpro" && (
        <img
          className="w-full h-[56.25vw] object-cover brightness-[60%] transition duration-500"
          srcSet={`${data.cover_image_medium} 400w, ${data.cover_image_large} 1024w, ${data.cover_image_1536} 1536w, ${data.cover_image_full} 1920w`}
          src={data.cover_image_large || "/images/empty.png"}
          alt={data.post_title}
        />
      )}

      <div className="absolute bottom-2 md:bottom-10 ml-4 md:ml-16">
        <p className="text-white text-md md:text-5xl h-full w-full md:w-[70%] md:w-[50%] lg:text-6xl font-bold drop-shadow-xl line-clamp-1 md:line-clamp-3">
          {data.post_title}
        </p>
        <div
          className="text-white text-lg mt-3 mt-8 w-[80%] lg:w-[50%] drop-shadow-xl max-md:hidden line-clamp-4"
          dangerouslySetInnerHTML={{ __html: data.post_content }}
        ></div>
        <div className="flex flex-row items-center mt-1 sm:mt-4 gap-3">
          <PlayButton
            itemSlug={data.post_name}
            mediaType={data.post_type}
            isBillboard={true}
          />
          {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
            <DownloadButton data={data} isBillboard={true} />
          )}
          <button
            onClick={handleOpenModal}
            className="
              bg-green-500
              text-white
              bg-opacity-80 
              rounded-md 
              py-1 md:py-2 
              px-2 md:px-4
              w-auto 
              text-xs lg:text-lg 
              font-semibold
              flex
              flex-row
              items-center
              hover:bg-opacity-100
              transition
            "
          >
            <InformationCircleIcon className="w-4 md:w-7 mr-1" />
            Sobre
          </button>
        </div>
      </div>
    </div>
  );
};
export default Billboard;
