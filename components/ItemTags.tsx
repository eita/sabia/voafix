import React, { useCallback } from "react";
import { useRouter } from "next/router";
import { ChevronDownIcon } from "@heroicons/react/24/outline";
import { PlayIcon } from "@heroicons/react/24/solid";

import { ItemInterface, TaxonomyTypeExtended } from "@/types";
import FavoriteButton from "@/components/FavoriteButton";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import dayjs from "dayjs";

interface ItemTagsProps {
  data: ItemInterface;
  taxonomies?: TaxonomyTypeExtended[] | null;
}

const ItemTags: React.FC<ItemTagsProps> = ({ data, taxonomies }) => {
  if (
    (!data.tax_decada || data.tax_decada.length === 0) &&
    (!data.tax_tema || data.tax_tema.length === 0) &&
    (!data.tax_wpdmcategory || data.tax_wpdmcategory.length === 0)
  ) {
    return <></>;
  }

  let postType: string;
  switch (data.post_type) {
    case "audio":
      postType = "AUDIO";
      break;
    case "video":
      postType = "VIDEO";
      break;
    case "wpdmpro":
    default:
      postType = "PDF";
      break;
  }

  const chipClass =
    "text-black text-[10px] md:text-sm bg-red-50 p-0.5 md:p-1 rounded-lg md:mr-2 md:mb-2 mr-1 mb-1 whitespace-nowrap max-md:overflow-hidden max-md:text-ellipsis";

  return (
    <div className="w-full flex flex-wrap">
      {!taxonomies ||
        (taxonomies.includes("postType") && (
          <p key="postType" className={chipClass}>
            {postType}
          </p>
        ))}
      {data.tax_decada &&
        (!taxonomies || taxonomies.includes("decada")) &&
        data.tax_decada.map((tag) => (
          <p key={tag.slug} className={chipClass}>
            {tag.name}
          </p>
        ))}
      {data.tax_tema &&
        (!taxonomies || taxonomies.includes("tema")) &&
        data.tax_tema.map((tag) => (
          <p key={tag.slug} className={chipClass}>
            {tag.name}
          </p>
        ))}
      {data.tax_wpdmcategory &&
        (!taxonomies || taxonomies.includes("wpdmcategory")) &&
        data.tax_wpdmcategory.map((tag) => (
          <p key={tag.slug} className={chipClass}>
            {tag.name}
          </p>
        ))}
    </div>
  );
};

export default ItemTags;
