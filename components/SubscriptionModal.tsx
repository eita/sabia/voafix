import React from "react";

interface SubscriptionModalProps {
  visible: boolean;
  handleClose: () => void;
}

const SubscriptionModal: React.FC<SubscriptionModalProps> = ({
  visible,
  handleClose,
}) => {
  if (!visible) {
    return <></>;
  }
  return (
    <div
      id="modelConfirm"
      className="fixed z-50 inset-0 bg-gray-900 bg-opacity-60 overflow-y-auto h-full w-full px-4 "
    >
      <div className="modal-subscription relative top-40 mx-auto shadow-xl rounded-md bg-white w-11/12 max-w-lg h-4/5">
        <div className="absolute right-0 flex justify-end p-2">
          <button
            type="button"
            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm ml-auto inline-flex items-center"
            onClick={handleClose}
          >
            <svg
              className="w-5 h-5"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>
        </div>

        <div className="text-center h-full">
          <iframe
            className="w-full h-full rounded-md"
            src="https://centrosabia.org.br/cadastro-voaflix/"
          />
        </div>
      </div>
    </div>
  );
};

export default SubscriptionModal;
