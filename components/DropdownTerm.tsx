import React, { Fragment } from "react";
import { ChevronDownIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { Menu, Transition } from "@headlessui/react";

import { TaxonomyTypeExtended } from "@/types";
import { classNames } from "@/libs/classNames";
import useTaxonomyList from "@/hooks/useTaxonomyList";
import { useRouter } from "next/router";

interface DropdownTermProps {
  showBackground: boolean;
  taxonomy: TaxonomyTypeExtended;
  termSlug?: string;
  termName?: string;
  basePath?: string;
  postPath: string;
}

const DropdownTerm: React.FC<DropdownTermProps> = ({
  taxonomy,
  termSlug,
  termName,
  showBackground,
  basePath,
  postPath,
}) => {
  const router = useRouter();
  const { data: termList } = useTaxonomyList(taxonomy);

  let placeholder = termName;
  if (!placeholder) {
    switch (taxonomy) {
      case "decada":
        placeholder = "Década";
        break;
      case "postType":
        placeholder = "Mídia";
        break;
      case "tema":
        placeholder = "Tema";
        break
      case "wpdmcategory":
        placeholder = "Tipo";
        break;
    }
  }

  if (!termList) {
    return <></>;
  }

  const baseUrl = basePath ? `/${basePath}&post_type=` : `/${taxonomy}/?id=`;

  return (
    <Menu as="div" className="md:relative inline-block text-left">
      {({ open, close }) => (
        <>
          <div>
            <Menu.Button
              className={classNames(
                showBackground
                  ? "hover:text-white ring-gray-300"
                  : "ring-white",
                "hover:bg-green-300 inline-flex w-full justify-center gap-x-1.5 px-1.5 py-1 text-xs md:px-3 md:py-2 md:text-sm font-semibold shadow-sm ring-1 md:ring-2 ring-inset rounded-md",
                open
                  ? "bg-green-300 text-white"
                  : showBackground
                    ? "text-zinc-800"
                    : "text-white"
              )}
            >
              {placeholder}
              <ChevronDownIcon
                className={classNames("-mr-1 h-5 w-5")}
                aria-hidden="true"
              />
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items
              className={classNames(
                "md:rounded-md bg-white-100 md:bg-opacity-90",
                "absolute right-0 max-md:w-[100vw] max-md:h-[100vh] max-md:top-0 z-[100] md:z-10 md:mt-2 max-md:pb-14 origin-top-right focus:outline-none",
                taxonomy == "tema" || taxonomy == "wpdmcategory"
                  ? "md:w-72 md:h-[50vh] overflow-hidden overflow-y-auto"
                  : taxonomy == "decada"
                    ? "md:w-28"
                    : "md:w-36"
              )}
            >
              <div
                className={classNames(
                  "md:grid flex flex-col items-center md:items-start gap-2",
                  taxonomy == "postType" || taxonomy == "decada"
                    ? `md:grid-cols-1 md:py-2`
                    : taxonomy == "wpdmcategory"
                      ? `md:grid-cols-1 md:p-2`
                      : "md:grid-cols-1 md:p-2"
                )}
              >
                {termList.map((term) => (
                  <Menu.Item key={`${taxonomy}_${term.slug}`}>
                    {({ active }) => (
                      <button
                        onClick={() => {
                          router.push(
                            `${baseUrl}${term.term_id || term.slug}${postPath}`
                          );
                        }}
                        className={classNames(
                          active ? "bg-green-300 text-white" : "text-gray-700",
                          "block px-2 py-1 text-xs md:px-4 md:py-2 md:text-sm text-left overflow-hidden whitespace-nowrap text-ellipsis hover:whitespace-normal",
                          taxonomy == "decada" ? "text-center" : "",
                          term.slug === termSlug ? "font-semibold" : ""
                        )}
                      >
                        {term.name}
                      </button>
                    )}
                  </Menu.Item>
                ))}
              </div>
            </Menu.Items>
          </Transition>
          {open && (
            <div
              onClick={close}
              className="md:hidden cursor-pointer fixed bottom-3 h-10 w-10 rounded-full bg-zinc-800 bg-opacity-70 flex items-center justify-center z-[150]"
            >
              <XMarkIcon className="text-white w-6" />
            </div>
          )}
        </>
      )}
    </Menu>
  );
};
export default DropdownTerm;
