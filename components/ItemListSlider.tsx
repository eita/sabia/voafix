import React, { useEffect, useRef } from "react";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline";

import { ItemInterface, TaxonomyTypeExtended } from "@/types";
import ItemCard from "@/components/ItemCard";
import useOnScreen from "@/hooks/useOnScreen";
import useItemList from "@/hooks/useItemList";
import useSliding from "@/hooks/useSliding";
import LoadingList from "./LoadingList";
import { classNames } from "@/libs/classNames";

interface ItemListSliderProps {
  taxonomy?: TaxonomyTypeExtended | "tema";
  term?: string | null;
  title?: string | null;
}

const ItemListSlider: React.FC<ItemListSliderProps> = ({
  taxonomy,
  term,
  title,
}) => {
  const ref = useRef<HTMLDivElement | null>(null);
  const isVisible = useOnScreen(ref);

  const { data, error, size, setSize, isValidating } = useItemList({
    taxonomy,
    term: term || null,
    title: title || null,
  });

  const emptyArray: ItemInterface[] = [];
  const itemList = data ? emptyArray.concat(...data) : [];
  const isLoadingInitialData = !data && !error;
  const isReachingEnd =
    data && data.length > 0 && data[data.length - 1].length < 20;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && data && !isReachingEnd);
  const isRefreshing = isValidating && data && data.length === size;

  const { handlePrev, handleNext, slideProps, containerRef, hasNext, hasPrev } =
    useSliding(20, 1, itemList ? itemList.length : 0);

  useEffect(() => {
    if ((!hasNext || isVisible) && isLoadingMore && !isLoadingInitialData) {
      setSize((prevSize: number) => prevSize + 1);
    }
  }, [hasNext, isVisible, isLoadingMore, isLoadingInitialData, setSize]);

  if (!itemList || !taxonomy || !term) {
    return <></>;
  }

  return (
    <div className="px-4 md:px-0 mt-4 space-y-8">
      <div>
        <p className="text-white text-md md:text-xl lg:text-2xl font-semibold mb-4 md:mb-0 md:px-12">
          {title}
        </p>
        <div className="flex flex-row items-stretch">
          <div
            className={`w-12 max-md:hidden pt-4 pb-32 flex ${
              hasPrev ? "" : "invisible"
            }`}
          >
            <button
              className="text-green-300 w-12"
              onClick={() => {
                handlePrev();
              }}
            >
              <ChevronLeftIcon className="w-11 hover:w-12 transition" />
            </button>
          </div>

          <div className="w-full overflow-x-auto overflow-y-clip no-scrollbar max-md:snap-mandatory max-md:snap-x md:overflow-hidden relative transition md:pt-4 md:pb-32">
            <div ref={containerRef} className="flex relative">
              {isLoadingInitialData ? (
                <div className="w-full flex items-center justify-center">
                  <LoadingList />
                </div>
              ) : (
                <div
                  className="flex gap-4 z-3 w-full transition"
                  {...slideProps}
                >
                  {itemList.map((item) => (
                    <ItemCard key={item.ID} data={item} />
                  ))}
                  <div
                    ref={ref}
                    className={classNames(
                      "md:hidden w-[8rem] shrink-0 flex flex-row items-center justify-center",
                      isLoadingInitialData || isReachingEnd
                        ? "invisible"
                        : "snap-x"
                    )}
                  >
                    <LoadingList size={10} />
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className={`w-12 max-md:hidden pt-4 pb-32 flex`}>
            {isLoadingMore && !hasNext ? (
              <div
                className={`w-12 flex items-center ${
                  isLoadingInitialData ? "invisible" : ""
                }`}
              >
                <LoadingList size={10} />
              </div>
            ) : (
              <button
                className={`text-green-300 w-12 ${hasNext ? "" : "invisible"}`}
                onClick={() => {
                  handleNext();
                }}
              >
                <ChevronRightIcon className="w-11 hover:w-12 transition" />
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemListSlider;
