import React from "react";

import { ItemInterface } from "@/types";
import { isEmpty } from "lodash";
import ItemCardInGrid from "./ItemCardInGrid";

interface ItemListGridProps {
  data: ItemInterface[] | undefined;
  title: string;
  emptyMessage: string;
  loadingRef: any;
  isLoadingMore: boolean | null;
  isEmpty: boolean;
}

const ItemListGrid: React.FC<ItemListGridProps> = ({
  data,
  title,
  emptyMessage,
  loadingRef,
  isLoadingMore,
  isEmpty,
}) => {
  return (
    <div className="px-4 md:px-12 mt-4 space-y-8">
      {/* title && (
        <div className="text-zinc-800 font-bold text-3xl mt-10">{title}</div>
      ) */}
      <div className="grid grid-cols-1 md:grid-cols-4 gap-3 md:gap-4">
        {data &&
          data.length > 0 &&
          data.map((item) => <ItemCardInGrid key={item.ID} data={item} />)}
        {isEmpty && (
          <div className="absolute inset-0 bg-zinc-800 text-white text-xl md:text-3xl px-12 pt-[150px] md:pt-[300px] md:pl-24 pb-12">
            {emptyMessage}
          </div>
        )}
      </div>
      <div
        ref={loadingRef}
        className="h-24 w-full flex items-center justify-center"
      >
        {isLoadingMore && (
          <div
            className="inline-block h-8 w-8 animate-spin rounded-full border-4 border-solid border-current border-e-transparent align-[-0.125em] text-surface motion-reduce:animate-[spin_1.5s_linear_infinite] text-red-300"
            role="status"
          >
            <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
              Carregando...
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ItemListGrid;
