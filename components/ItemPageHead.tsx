import React from "react";
import Head from "next/head";
import Script from "next/script";

import { ActionType, ItemInterface } from "@/types";
import { cleanHTML } from "@/libs/cleanHTML";

interface ItemPageHeadProps {
  data: ItemInterface;
  action: ActionType;
  slug: string;
}

const ItemPageHead: React.FC<ItemPageHeadProps> = ({ data, action, slug }) => {
  const description = cleanHTML(data.post_content);
  return (
    <Head>
      <title>VoaFlix - {data.post_title}</title>
      <meta name="description" content={description} />
      <meta property="og:title" content={`VoaFlix - ${data.post_title}`} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={data.featured_image_thumb} />
      <meta
        property="og:url"
        content={`https://voaflix.centrosabia.org.br/${action}/${slug}`}
      />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="Voaflix" />
    </Head>
  );
};

export default ItemPageHead;
