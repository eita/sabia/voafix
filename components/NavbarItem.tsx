import { classNames } from "@/libs/classNames";
import { useRouter } from "next/router";
import React from "react";

interface NavbarItemProps {
  label: string;
  active?: boolean;
  showBackground?: boolean;
  decadaId?: number;
}

const NavbarItem: React.FC<NavbarItemProps> = ({
  label,
  active,
  showBackground,
  decadaId,
}) => {
  const router = useRouter();
  return (
    <button
      className={classNames(
        "cursor-pointer transition font-semibold max-md:text-sm",
        showBackground
          ? "text-red-300 hover:text-red-400"
          : "text-white-300 hover:text-gray-300"
      )}
      onClick={() => {
        if (!decadaId) {
          router.push("/");
          return;
        }
        router.push(`/decada?id=${decadaId}`);
      }}
    >
      {label}
    </button>
  );
};

export default NavbarItem;
