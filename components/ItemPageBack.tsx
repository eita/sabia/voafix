import React from "react";
import { ArrowLeftIcon } from "@heroicons/react/24/outline";

import useNavigationHistory from "@/hooks/useNavigationHistory";

const ItemPageBack: React.FC = () => {
  const { handleBackNavigation } = useNavigationHistory();
  return (
    <ArrowLeftIcon
      onClick={handleBackNavigation}
      className="w-10 text-white cursor-pointer hover:opacity-80 transition"
    />
  );
};

export default ItemPageBack;
