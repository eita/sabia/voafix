import React, { useEffect, useRef } from "react";

import Navbar from "@/components/Navbar";
import Billboard from "@/components/Billboard";
import InfoModal from "@/components/InfoModal";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import { ItemInterface, ItemType, TaxonomyTypeExtended } from "@/types";
import useTerm from "@/hooks/useTerm";
import useItemList from "@/hooks/useItemList";
import ItemListGrid from "@/components/ItemListGrid";
import useOnScreen from "@/hooks/useOnScreen";
import { getPostTypeName } from "@/libs/getPostTypeName";
import Head from "next/head";
import Footer from "./Footer";

interface PageWithFiltersContentProps {
  taxonomy: TaxonomyTypeExtended;
  termId?: number;
  termSlug?: string;
  emptyMessage: string;
  title?: string;
  postType?: ItemType;
}

const PageWithFiltersContent: React.FC<PageWithFiltersContentProps> = ({
  taxonomy,
  termId,
  termSlug,
  emptyMessage,
  title,
  postType,
}) => {
  const { data: term } = useTerm(termId || null);
  const ref = useRef<HTMLDivElement>(null);
  const isVisible = useOnScreen(ref);

  let termSlugFinal: string | null = null;
  let termNameFinal: string | null = null;
  let pageTitle: string = "";

  switch (taxonomy) {
    case "search":
      termSlugFinal = termSlug || null;
      termNameFinal = termSlug || null;
      pageTitle = `Busca por "${termSlug}"`;
      break;
    case "postType":
      termSlugFinal = termSlug || null;
      termNameFinal = getPostTypeName((termSlug as ItemType) || null);
      pageTitle = `Mídia: ${termNameFinal}`;
      break;
    default:
      termSlugFinal = term ? term.slug : null;
      termNameFinal = term ? term.name : null;
      pageTitle = !termNameFinal
        ? ""
        : taxonomy === "decada"
          ? `Década de ${termNameFinal}`
          : termNameFinal;
  }

  const useItemListProps =
    taxonomy === "search"
      ? { taxonomy, search: termSlug, title: termSlug || null }
      : {
        taxonomy,
        term: termSlugFinal,
        title: termNameFinal,
      };

  const { data, error, size, setSize, isValidating } = useItemList(
    useItemListProps,
    postType
  );

  const { isOpen, closeModal } = useInfoModalStore();

  const emptyArray: ItemInterface[] = [];
  const itemList = data ? emptyArray.concat(...data) : [];
  const isLoadingInitialData = !data && !error;
  const isEmpty = !!data && data[0].length === 0;
  const isReachingEnd =
    data && data.length > 0 && data[data.length - 1].length < 20;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && data && !isReachingEnd);
  const isRefreshing = isValidating && data && data.length === size;

  useEffect(() => {
    if (isVisible && !isReachingEnd) {
      setSize((prevSize: number) => prevSize + 1);
    }
  }, [isVisible, isReachingEnd, setSize]);

  const terms = [
    {
      taxonomy,
      termSlug: termSlugFinal || "",
      termName: termNameFinal || "",
      termId,
    },
  ];
  let emptyMessageFinal = termNameFinal
    ? emptyMessage.replace("{TERM}", termNameFinal)
    : "";
  if (postType) {
    const postTypeName = getPostTypeName(postType) || "";
    terms.push({
      taxonomy: "postType",
      termSlug: postType,
      termName: postTypeName,
      termId: undefined,
    });
    emptyMessageFinal = emptyMessageFinal.replace("{POST_TYPE}", postTypeName);
    pageTitle += ` (Mídia: ${postTypeName})`;
  }
  const itemListTitle = title
    ? title.replace("{TERM}", termNameFinal || "")
    : termNameFinal || "";

  return (
    <>
      <Head>
        <title>VoaFlix - {pageTitle}</title>
      </Head>
      <InfoModal visible={isOpen} onClose={closeModal} />
      {termSlugFinal && (
        <>
          <Navbar terms={terms} title={itemListTitle} user={null} />
          {!isEmpty && (
            <Billboard
              taxonomy={taxonomy}
              termSlug={termSlugFinal}
              postType={postType}
            />
          )}
        </>
      )}
      <div>
        <ItemListGrid
          title={itemListTitle}
          data={itemList}
          emptyMessage={emptyMessageFinal}
          loadingRef={ref}
          isLoadingMore={isLoadingMore}
          isEmpty={isEmpty}
        />
      </div>
      <Footer />
    </>
  );
};

export default PageWithFiltersContent;
