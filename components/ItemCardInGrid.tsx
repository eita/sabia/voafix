import { useCallback } from "react";
import { useRouter } from "next/router";
import { InformationCircleIcon } from "@heroicons/react/24/outline";

import { ItemInterface } from "@/types";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import ItemTags from "./ItemTags";
import PlayButton from "./PlayButton";
import DownloadButton from "./DownloadButton";

interface MovieCardProps {
  data: ItemInterface;
}

const ItemCardInGrid: React.FC<MovieCardProps> = ({ data }) => {
  const router = useRouter();
  const { openModal } = useInfoModalStore();
  
  const redirectToItemPage = useCallback(() => {
    let path = "";
    switch (data.post_type) {
      case "audio":
        path = "escute";
        break;
      case "video":
        path = "assista";
        break;
      case "wpdmpro":
        path = "leia";
        break;
    }
    return router.push(`/${path}/${data.post_name}`);
  }, [router, data.post_name, data.post_type]);

  return (
    <div className="group bg-white-50 w-full col-span relative hover:z-50 rounded-md max-md:shadow-xl">
      <div className="md:hidden">
        <img
          onClick={() => {
            openModal(data.ID);
          }}
          src={data.featured_image_full || "/images/empty.png"}
          alt={data.post_title}
          draggable={false}
          className="cursor-pointer object-cover rounded-t-md w-full h-full md:h-[30vw]"
        />
        <div className="p-2 w-full rounded-b-md">
          <div className="flex flex-row items-center justify-between">
            <PlayButton
              itemSlug={data.post_name}
              mediaType={data.post_type}
              noLabel={false}
            />
            {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
              <DownloadButton data={data} noLabel={true} />
            )}
            <button
              title="Sobre"
              onClick={() => openModal(data.ID)}
              className="
              bg-green-300
              border
              border-green-300
              text-white
              bg-opacity-100 
              py-1
              px-2
              w-auto 
              text-xs lg:text-md
              font-semibold
              flex
              flex-row
              items-center
              hover:bg-opacity-80
              transition
            "
            >
              <InformationCircleIcon className="w-4 md:w-7 mr-1" />
            </button>
          </div>
          <div className="pt-3">
            <ItemTags data={data} />
          </div>
        </div>
      </div>
      <img
        onClick={redirectToItemPage}
        src={data.featured_image_full || "/images/empty.png"}
        alt={data.post_title}
        draggable={false}
        className="
        cursor-pointer
        object-cover
        transition
        duration
        shadow-xl
        rounded-md
        group-hover:opacity-0
        max-md:hidden
        delay-300
        w-full
        h-[12vw]
      "
      />
      <div
        className="
        opacity-0
        absolute
        top-0
        transition
        duration-200
        z-10
        delay-300
        w-full
        scale-0
        group-hover:scale-110
        group-hover:-translate-y-[6vw]
        group-hover:translate-x-[2vw]
        group-hover:opacity-100
        max-md:hidden
      "
      >
        <img
          onClick={redirectToItemPage}
          src={data.featured_image_full || "/images/empty.png"}
          alt={data.post_title}
          draggable={false}
          className="
          cursor-pointer
          object-cover
          transition
          duration
          shadow-xl
          rounded-t-md
          w-full
          h-auto
        "
        />
        <div
          className="
          z-10
          bg-white-50
          p-2
          lg:p-4
          absolute
          w-full
          transition
          shadow-md
          rounded-b-md
          "
        >
          <div className="flex flex-row items-center gap-3">
            <PlayButton itemSlug={data.post_name} mediaType={data.post_type} />
            {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
              <DownloadButton data={data} noLabel={true} />
            )}
            <button
              onClick={() => openModal(data.ID)}
              className="
              bg-red-300
              text-white
              bg-opacity-100 
              py-1
              px-2
              w-auto 
              text-xs lg:text-md
              font-semibold
              flex
              flex-row
              items-center
              hover:bg-opacity-80
              transition
            "
            >
              <InformationCircleIcon className="w-4 md:w-7 mr-1" />
              Sobre
            </button>
          </div>
          <div className="pt-3">
            <ItemTags data={data} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemCardInGrid;
