import React, { useCallback } from "react";
import { useRouter } from "next/router";
import { InformationCircleIcon } from "@heroicons/react/24/outline";

import { ItemInterface } from "@/types";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import ItemTags from "./ItemTags";
import PlayButton from "./PlayButton";
import DownloadButton from "./DownloadButton";

interface ItemCardProps {
  data: ItemInterface;
}

const ItemCard: React.FC<ItemCardProps> = ({ data }) => {
  const router = useRouter();
  const { openModal } = useInfoModalStore();

  const redirectToItemPage = useCallback(() => {
    let path = "";
    switch (data.post_type) {
      case "audio":
        path = "escute";
        break;
      case "video":
        path = "assista";
        break;
      case "wpdmpro":
        path = "leia";
        break;
    }
    return router.push(`/${path}/${data.post_name}`);
  }, [router, data.post_name, data.post_type]);

  return (
    <div className="group bg-white-50 rounded-md col-span relative max-md:snap-center shrink-0 w-[70vw] md:w-auto hover:z-40">

      <div className="md:hidden w-full h-full">
        <img
          className="cursor-pointer object-cover rounded-t-md h-auto md:h-[24vw] w-full"
          onClick={() => {
            openModal(data.ID);
          }}
          src={data.featured_image_full || "/images/empty.png"}
          alt={data.post_title}
          draggable={false}
        />
        <div className="p-2 w-full rounded-b-md">
          <div className="flex flex-row items-center justify-between">
            <PlayButton
              itemSlug={data.post_name}
              mediaType={data.post_type}
              noLabel={false}
            />
            {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
              <DownloadButton data={data} noLabel={true} />
            )}
            <button
              title="Sobre"
              onClick={() => openModal(data.ID)}
              className="
              bg-green-300
              border
              border-green-300
              text-white
              bg-opacity-100 
              py-1
              px-2
              w-auto 
              text-xs lg:text-md
              font-semibold
              flex
              flex-row
              items-center
              hover:bg-opacity-80
              transition
            "
            >
              <InformationCircleIcon className="w-4 md:w-7 mr-1" />
            </button>
          </div>
          <div className="pt-3">
            <ItemTags data={data} />
          </div>
        </div>
      </div>
      <img
        src={data.featured_image_full || "/images/empty.png"}
        alt={data.post_title}
        draggable={false}
        className="
        max-md:hidden
        cursor-pointer
        object-cover
        transition
        duration
        shadow-xl
        rounded-md
        group-hover:opacity-0
        delay-300
        w-full
        h-[12vw]
      "
      />
      <div
        className="
        max-md:hidden
        opacity-0
        absolute
        top-0
        transition
        duration-200
        z-10
        delay-300
        w-full
        scale-0
        group-hover:scale-100
        group-hover:-translate-y-[1vw]
        group-hover:opacity-100
      "
      >
        <img
          onClick={redirectToItemPage}
          src={data.featured_image_full || "/images/empty.png"}
          alt={data.post_title}
          draggable={false}
          className="
          cursor-pointer
          object-cover
          transition
          duration
          shadow-xl
          rounded-t-md
          w-full
          h-auto
        "
        />
        <div
          className="
          bg-white-50
          px-2
          pt-2
          lg:px-4
          lg:pt-4
          w-full
          transition
          shadow-md
          rounded-b-md
          "
        >
          <div className="flex flex-row items-center gap-3">
            <PlayButton itemSlug={data.post_name} mediaType={data.post_type} />
            {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
              <DownloadButton data={data} noLabel={true} />
            )}
            <button
              onClick={() => openModal(data.ID)}
              className="
              bg-red-300
              text-white
              bg-opacity-100 
              py-1
              px-2
              w-auto 
              text-xs lg:text-md
              font-semibold
              flex
              flex-row
              items-center
              hover:bg-opacity-80
              transition
            "
            >
              <InformationCircleIcon className="w-4 md:w-7 mr-1" />
              Sobre
            </button>
          </div>
          <div className="pt-3">
            <ItemTags data={data} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemCard;
