import React, { useCallback, useEffect, useState } from "react";
import { XMarkIcon } from "@heroicons/react/24/outline";

import PlayButton from "@/components/PlayButton";
import FavoriteButton from "@/components/FavoriteButton";
import useInfoModalStore from "@/hooks/useInfoModalStore";
import useItem from "@/hooks/useItem";
import ItemTags from "./ItemTags";
import DownloadButton from "./DownloadButton";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import { getPostTypeName } from "@/libs/getPostTypeName";

interface InfoModalProps {
  visible?: boolean;
  onClose: any;
}

const InfoModal: React.FC<InfoModalProps> = ({ visible, onClose }) => {
  const [isVisible, setIsVisible] = useState<boolean>(!!visible);
  const router = useRouter();

  const { movieId } = useInfoModalStore();
  const { data } = useItem(movieId);

  useEffect(() => {
    setIsVisible(!!visible);
  }, [visible]);

  const handleClose = useCallback(() => {
    setIsVisible(false);
    setTimeout(() => {
      onClose();
    }, 300);
  }, [onClose]);

  useEffect(() => {
    const handleUserKeyPress = (event: KeyboardEvent) => {
      const { key } = event;
      if (key === "Escape") {
        handleClose();
      }
    };

    window.addEventListener("keydown", handleUserKeyPress);

    return () => {
      window.removeEventListener("keydown", handleUserKeyPress);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const handleRouteChange = () => {
      if (isVisible) {
        handleClose();
      }
    };

    // Listen for the popstate event
    window.addEventListener("popstate", handleRouteChange);

    // Clean up the event listener
    return () => {
      window.removeEventListener("popstate", handleRouteChange);
    };
  }, [isVisible, handleClose]);

  if (!isVisible) {
    return <></>;
  }

  let formatoStr: string = "";
  if (data) {
    formatoStr = getPostTypeName(data.post_type) || "";
  }

  const specsRowClassesLeft = "bg-white-100 py-2 pl-2 font-semibold";
  const specsRowClassesRight = "bg-white-100 py-2 pl-4 pr-2";

  return (
    <div
      className="z-[100] transition duration-300 bg-white-50 bg-opacity-80 flex justify-center items-start overflow-x-hidden overflow-y-auto fixed inset-0"
      onClick={handleClose}
    >
      <div
        className="w-full md:w-auto md:mx-auto md:max-w-5xl"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div
          className={`${
            isVisible ? "scale-100" : "scale-0"
          } transform duration-300 bg-black drop-shadow-md md:mb-10 md:mt-10 md:rounded-md`}
        >
          <div className="h-96 relative bg-zinc-800">
            {data && data.post_type === "video" && data.external_id && (
              <iframe
                className="w-full brightness-[60%] object-cover h-full rounded-t-md"
                src={`https://www.youtube.com/embed/${data.external_id}?autoplay=1&fs=0&rel=0&mute=1`}
                title={data.post_title}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                allowFullScreen
              ></iframe>
            )}
            {data && data.post_type === "audio" && data.external_id && (
              <iframe
                className="w-full brightness-[60%] object-cover h-full"
                src={`https://open.spotify.com/embed/episode/${data.external_id}`}
                allowFullScreen
                allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
                loading="lazy"
                title={data.post_title}
              ></iframe>
            )}
            {data && data.post_type === "wpdmpro" && (
              <img
                className="w-full brightness-[60%] object-cover h-full"
                src={data.featured_image_full || "/images/empty.png"}
                alt={data.post_title}
              />
            )}
            <div
              onClick={handleClose}
              className="cursor-pointer absolute top-3 right-3 h-10 w-10 rounded-full bg-white-50 bg-opacity-70 flex items-center justify-center"
            >
              <XMarkIcon className="text-black w-6" />
            </div>
            {data && (
              <div className="absolute bottom-2 md:bottom-[10%] left-2 md:left-10">
                {/* <p className="text-white text-3xl md:text-4xl h-full lg:text-5xl font-bold mb-8">
                {data.post_title}
              </p> */}
                <div className="flex flex-row gap-4 items-center">
                  <PlayButton
                    itemSlug={data.post_name}
                    mediaType={data.post_type}
                    onPress={handleClose}
                  />
                  {data.post_type === "wpdmpro" && data.wpdmpro_file_url && (
                    <DownloadButton data={data} onPress={handleClose} />
                  )}
                  {/* <FavoriteButton movieId={data.ID} /> */}
                </div>
              </div>
            )}
          </div>
          {data && (
            <div className="relative px-12 py-8 grid grid-cols-1 md:grid-cols-2 gap-8 items-start">
              <div>
                <p className="text-white text-2xl font-bold">
                  {data.post_title}
                </p>
                <p
                  className="text-white text-lg pt-4"
                  dangerouslySetInnerHTML={{ __html: data.post_content }}
                ></p>
              </div>

              <div className="grid grid-cols-2 grid-cols-[max-content_1fr] gap-y-4">
                {data.post_type === "wpdmpro" && (
                  <div className={specsRowClassesLeft}>Tipo:</div>
                )}
                {data.post_type === "wpdmpro" && (
                  <div className={specsRowClassesRight}>
                    {data.tax_wpdmcategory.length > 0
                      ? data.tax_wpdmcategory[0].name
                      : "-"}
                  </div>
                )}

                <div className={specsRowClassesLeft}>Formato:</div>
                <div className={specsRowClassesRight}>{formatoStr}</div>

                <div className={specsRowClassesLeft}>Ano:</div>
                <div className={specsRowClassesRight}>
                  {dayjs(data.post_date).year()}
                </div>

                <div className={specsRowClassesLeft}>Tema(s):</div>
                <div className={specsRowClassesRight}>
                  <ItemTags data={data} taxonomies={["tema"]} />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default InfoModal;
