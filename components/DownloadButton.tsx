import { ArrowDownTrayIcon } from "@heroicons/react/24/solid";
import { useRouter } from "next/router";
import { ItemInterface, ItemType } from "@/types";
import Link from "next/link";

interface DownloadButtonProps {
  data: ItemInterface;
  isBillboard?: boolean;
  onPress?: () => void;
  noLabel?: boolean;
}

const DownloadButton: React.FC<DownloadButtonProps> = ({
  data,
  isBillboard,
  onPress,
  noLabel,
}) => {
  const handleClick = () => {
    if (onPress) {
      onPress();
    }
  };

  if (!data || !data.wpdmpro_file_url) {
    console.log("error!");
    return <></>;
  }

  return (
    <Link href={data.wpdmpro_file_url}>
      <button
        onClick={handleClick}
        title="Download"
        className={`
        bg-white 
        ${isBillboard ? "rounded-md" : ""}
        border
        border-red-300 
        text-red-300
        py-1 ${isBillboard ? "md:py-2" : ""}
        px-2 ${isBillboard ? "md:px-4" : ""}
        w-auto 
        text-xs ${isBillboard ? "lg:text-lg" : "lg:text-md"}
        font-semibold
        flex
        flex-row
        items-center
        hover:bg-neutral-300
        transition
        `}
      >
        <ArrowDownTrayIcon className="w-4 md:w-7 text-red-300 mr-1" />
        {noLabel ? "" : "Download"}
      </button>
    </Link>
  );
};

export default DownloadButton;
