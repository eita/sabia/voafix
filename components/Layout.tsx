import React, { ReactNode, useEffect } from "react";
import Head from "next/head";
import Script from "next/script";

interface LayoutProps {
  children: ReactNode;
}

// declare global {
//   interface Window {
//     _mtm: any;
//   }
// }

const Layout = ({ children }: LayoutProps) => {
  //   useEffect(() => {
  //     var _mtm = (window._mtm = window._mtm || []);
  //     _mtm.push({ "mtm.startTime": new Date().getTime(), event: "mtm.Start" });
  //     var d = document,
  //       g = d.createElement("script"),
  //       s = d.getElementsByTagName("script")[0];
  //     g.async = true;
  //     g.src = "https://piwik.eita.org.br/js/container_XynY6SRB.js";
  //     if (s.parentNode != null) {
  //       s.parentNode.insertBefore(g, s);
  //     }
  //   }, []);
  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1"
        />
      </Head>
      {/* Google Tag Manager - Head */}
      <Script
        id="google-tag-manager"
        strategy="afterInteractive" // Load the script after the page becomes interactive
      >
        {`(function(w,d,s,l,i) {
            w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
            f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-TH2CXNFB');`}
      </Script>

      {/* Google Tag Manager - Noscript */}
      <noscript>
        <iframe
          src={`https://www.googletagmanager.com/ns.html?id=GTM-TH2CXNFB`}
          height="0"
          width="0"
          style={{ display: "none", visibility: "hidden" }}
        />
      </noscript>
      {/* Google Site Verification */}
      <meta name="google-site-verification" content="nsoVamvHrHvH7z4FatSH0enqshH5Le2XzfnaWFQWuaA" />
      {children}
    </>
  );
};

export default Layout;
