import React, { useCallback, useEffect, useState } from "react";
import {
  // BellIcon,
  // ChevronDownIcon,
  XMarkIcon,
} from "@heroicons/react/24/outline";

import { MagnifyingGlassIcon } from "@heroicons/react/24/solid";

import NavbarItem from "@/components/NavbarItem";
import useTaxonomyList from "@/hooks/useTaxonomyList";
import { useRouter } from "next/router";
import { NavbarTerm, User } from "@/types";
import DropdownTerm from "./DropdownTerm";
import { classNames } from "@/libs/classNames";
import Link from "next/link";
import NavbarFilterTitle from "./NavbarFilterTitle";

const TOP_OFFSET = 66;

interface NavbarProps {
  terms?: NavbarTerm[];
  title?: string;
  user: User | null;
}

const Navbar = ({ terms, title, user }: NavbarProps) => {
  const selectedTema: NavbarTerm | undefined = terms
    ? terms.filter((term) => term.taxonomy === "tema")[0]
    : undefined;

  const selectedPostType: NavbarTerm | undefined = terms
    ? terms.filter((term) => term.taxonomy === "postType")[0]
    : undefined;

  const selectedDecada: NavbarTerm | undefined = terms
    ? terms.filter((term) => term.taxonomy === "decada")[0]
    : undefined;

  const selectedTipo: NavbarTerm | undefined = terms
    ? terms.filter((term) => term.taxonomy === "wpdmcategory")[0]
    : undefined;

  const s = terms
    ? terms.filter((term) => term.taxonomy === "search")[0]
    : undefined;

  // const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [showBackground, setShowBackground] = useState(false);
  const [searchBarOpen, setSearchBarOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState(
    s && s.termSlug ? s.termSlug : ""
  );
  const router = useRouter();

  const { data: temas } = useTaxonomyList("tema", true);
  const { data: decadas } = useTaxonomyList("decada");

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY >= TOP_OFFSET) {
        setShowBackground(true);
      } else {
        setShowBackground(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    handleSearchBarClose();
    doSubmit();
  };

  const doSubmit = () => {
    let url = `/search/?s=${searchQuery.trim()}`;
    if (selectedPostType) {
      url += `&post_type=${selectedPostType.termSlug}`;
    }
    router.push(url);
  };

  // const toggleMobileMenu = useCallback(() => {
  //   setShowMobileMenu((current) => !current);
  // }, []);

  const handleSearchBarClose = useCallback(() => {
    setSearchBarOpen(false);
  }, []);

  const handleSearchBarOpen = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    setSearchBarOpen(true);
  };

  return (
    <>
      <div
        className={classNames(
          searchBarOpen
            ? "z-[30] absolute top-0 left-0 w-screen h-screen bg-zinc-800 opacity-[90%]"
            : "hidden"
        )}
      ></div>
      <div
        onClick={handleSearchBarClose}
        className={classNames(
          "z-[60] cursor-pointer fixed top-2 right-4 h-8 w-8 rounded-full bg-opacity-70 flex items-center justify-center",
          showBackground ? "bg-zinc-800" : "bg-white-50",
          searchBarOpen ? "" : "hidden"
        )}
      >
        <XMarkIcon
          className={classNames(
            "w-6",
            showBackground ? "text-white-300" : "text-black"
          )}
        />
      </div>
      <nav className="w-full fixed z-40">
        <div
          className={classNames(
            "transition duration-500",
            showBackground ? "bg-black" : ""
          )}
        >
          <div
            className={classNames(
              "px-4 md:px-16 py-2 md:py-6 transition duration-500",
              showBackground ? "shadow-lg" : "",
              searchBarOpen ? "" : "flex flex-row items-center"
            )}
          >
            <Link href="/">
              <img
                src={
                  showBackground ? "/images/logo.png" : "/images/logo_white.png"
                }
                className={classNames(
                  "h-12 md:h-14",
                  searchBarOpen ? "opacity-40" : "",
                  showBackground ? "h-8" : "h-12"
                )}
                alt="Logo"
              />
            </Link>
            <div
              className={classNames(
                "flex-row ml-2 gap-1.5 md:gap-2 lg:ml-8 lg:gap-7 flex",
                searchBarOpen ? "hidden" : ""
              )}
            >
              {/* <NavbarItem label="Início" showBackground={showBackground} /> */}
              {showBackground &&
                decadas &&
                decadas.map((decada) => (
                  <NavbarItem
                    key={decada.slug}
                    label={decada.name}
                    showBackground={showBackground}
                    decadaId={decada.term_id}
                  />
                ))}
              {/* <NavbarItem label="Já vi" showBackground={showBackground} /> */}
            </div>
            <div
              className={classNames(
                searchBarOpen
                  ? ""
                  : "flex flex-row ml-auto justify-between md:gap-7",
                "items-center"
              )}
            >
              <form
                onSubmit={handleSubmit}
                className="relative flex flex-row items-start"
              >
                <input
                  type="text"
                  value={searchQuery}
                  onChange={handleChange}
                  placeholder="Buscar conteúdo"
                  className={classNames(
                    "bg-white border border-white focus:outline-none focus:border-red-300 transition rounded-l-md",
                    searchBarOpen
                      ? "p-2 absolute top-12 w-[calc(100%-2.5rem)]"
                      : "w-96 max-md:hidden px-4 py-2"
                  )}
                />
                <button
                  type="submit"
                  className={classNames(
                    "text-white bg-red-300 border border-red-300 transition delay-300 rounded-r-md",
                    searchBarOpen
                      ? "absolute top-12 right-0 w-10"
                      : "max-md:hidden"
                  )}
                  onClick={doSubmit}
                >
                  <MagnifyingGlassIcon className="w-10 p-2" />
                </button>
                {user && (
                  <div
                    className={classNames(
                      showBackground ? "text-red-300" : "text-white-300",
                      "p-2"
                    )}
                  >
                    Olá {user.name}!
                  </div>
                )}
              </form>
              <button
                className={classNames(
                  searchBarOpen ? "hidden" : "",
                  showBackground ? "text-red-300" : "text-white-300",
                  "p-1 md:hidden"
                )}
                onClick={handleSearchBarOpen}
              >
                <MagnifyingGlassIcon className="w-6" />
              </button>
              {/* <div className="text-gray-200 hover:text-gray-300 cursor-pointer transition"><BellIcon className="w-6" /></div> */}
            </div>
          </div>
          {terms && terms.length > 0 ? (
            <div
              className={classNames(
                "w-full transition duration-500",
                showBackground ? "bg-white-300" : ""
              )}
            >
              <div
                className={classNames(
                  "px-4 md:px-16 py-1 sm:py-3 flex flex-col max-md:gap-2 md:flex-row items-start md:items-center justify-between",
                  searchBarOpen ? "hidden" : ""
                )}
              >
                <div
                  className={classNames(
                    showBackground ? "text-zinc-800" : "text-white-50"
                  )}
                >
                  <div className="flex flex-col">
                    <NavbarFilterTitle
                      showBackground={showBackground}
                      terms={terms}
                      title={`Busca: "${s ? s.termName : ""}"`}
                      searchQuery={s ? s.termName : undefined}
                    />
                    <NavbarFilterTitle
                      showBackground={showBackground}
                      terms={terms}
                      title={title}
                      selectedFilter={selectedDecada}
                    />
                    <NavbarFilterTitle
                      showBackground={showBackground}
                      terms={terms}
                      title={title}
                      selectedFilter={selectedTema}
                    />
                    <NavbarFilterTitle
                      showBackground={showBackground}
                      terms={terms}
                      title={`Mídia: ${selectedPostType ? selectedPostType.termName : ""
                        }`}
                      selectedFilter={selectedPostType}
                    />
                  </div>
                </div>
                <div className="flex flex-row gap-4">
                  <DropdownTerm
                    showBackground={showBackground}
                    taxonomy="postType"
                    termSlug={
                      selectedPostType ? selectedPostType.termSlug : undefined
                    }
                    termName={
                      selectedPostType ? selectedPostType.termName : undefined
                    }
                    basePath={
                      selectedDecada
                        ? `decada/?id=${selectedDecada.termId}`
                        : selectedTema
                          ? `tema/?id=${selectedTema.termId}`
                          : searchQuery
                            ? `search/?s=${searchQuery}`
                            : undefined
                    }
                    postPath={
                      selectedPostType
                        ? `&post_type=${selectedPostType.termSlug}`
                        : ""
                    }
                  />
                  <DropdownTerm
                    showBackground={showBackground}
                    taxonomy="decada"
                    termSlug={
                      selectedDecada ? selectedDecada.termSlug : undefined
                    }
                    termName={
                      selectedDecada ? selectedDecada.termName : undefined
                    }
                    postPath={
                      selectedPostType
                        ? `&post_type=${selectedPostType.termSlug}`
                        : ""
                    }
                  />
                  <DropdownTerm
                    showBackground={showBackground}
                    taxonomy="tema"
                    termSlug={selectedTema ? selectedTema.termSlug : undefined}
                    termName={selectedTema ? selectedTema.termName : undefined}
                    postPath={
                      selectedPostType
                        ? `&post_type=${selectedPostType.termSlug}`
                        : ""
                    }
                  />
                  <DropdownTerm
                    showBackground={showBackground}
                    taxonomy="wpdmcategory"
                    termSlug={selectedTipo ? selectedTipo.termSlug : undefined}
                    termName={selectedTipo ? selectedTipo.termName : undefined}
                    postPath={
                      selectedPostType
                        ? `&post_type=${selectedPostType.termSlug}`
                        : ""
                    }
                  />
                </div>
              </div>
            </div>
          ) : (
            <>
              {showBackground && temas && temas.length > 0 && (
                <div
                  className={classNames(
                    "px-4 md:px-16 py-1 sm:py-3 flex flex-row whitespace-nowrap gap-2 overflow-x-auto max-md:no-scrollbar",
                    searchBarOpen ? "hidden" : ""
                  )}
                >
                  {temas.map((tema) => (
                    <button
                      key={tema.slug}
                      className={classNames(
                        showBackground
                          ? "bg-red-300 text-white border border-red-300"
                          : "border border-white-300 text-white-300 hover:bg-red-300 hover:border-red-300",
                        "p-1 text-xs rounded-md transition duration-500 mb-2 mt-1"
                      )}
                      onClick={() => {
                        router.push(`/tema/?id=${tema.term_id}`);
                      }}
                    >
                      {tema.name}
                    </button>
                  ))}
                </div>
              )}
            </>
          )}
        </div>
      </nav>
    </>
  );
};

export default Navbar;
