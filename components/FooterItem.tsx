import { classNames } from "@/libs/classNames";
import { useRouter } from "next/router";
import React from "react";

interface FooterItemProps {
  label: string;
  active?: boolean;  
  taxonomy: string;
  termId?: string|number;
}

const FooterItem: React.FC<FooterItemProps> = ({
  label,
  active,
  taxonomy,
  termId,
}) => {
  const router = useRouter();
  return (
    <p
      className={classNames(
        "cursor-pointer transition max-md:text-sm text-left",        
      )}
      onClick={() => {
        router.push(`/${taxonomy}?id=${termId}`);
      }}
    >
      {label}
    </p>
  );
};

export default FooterItem;
