import useTaxonomyList from '@/hooks/useTaxonomyList';
import React from 'react';
import FooterItem from './FooterItem';

interface FooterProps {
}

const Footer: React.FC<FooterProps> = () => {

    const { data: wpdmcategory } = useTaxonomyList("wpdmcategory");
    const { data: decadas } = useTaxonomyList("decada");
    const { data: temas } = useTaxonomyList("tema");
    const postType = [{ slug: 'audio', name: 'Podcast' }, { slug: 'video', name: 'Vídeo' }, { slug: 'wpdmpro', name: 'Documento PDF' }];

    return (
        <footer>
            <div className='bg-red-400 text-white p-8'>
                <div className='md:flex'>
                    <div className='w-1/4 mb-8'>
                        <h2 className='text-xl lg:text-2xl font-semibold mb-2 md:px-12'>Mídia</h2>
                        <div className='md:px-12'>
                            {postType.map((term) => (
                                <FooterItem
                                    key={term.slug}
                                    label={term.name}
                                    taxonomy={"postType"}
                                    termId={term.slug}
                                />
                            ))}
                        </div>
                    </div>
                    <div className='w-2/4 mb-8'>
                        <h2 className='text-xl lg:text-2xl font-semibold mb-2 md:px-12'>Tipos</h2>
                        <div className='md:px-12 md:columns-2'>
                            {wpdmcategory.map((term) => (
                                <FooterItem
                                    key={term.slug}
                                    label={term.name}
                                    taxonomy={"wpdmcategory"}
                                    termId={term.term_id}
                                />
                            ))}
                        </div>
                    </div>
                    <div className='w-1/4'>
                        <h2 className='text-xl lg:text-2xl font-semibold mb-2 md:px-12'>Décadas</h2>
                        <div className='md:px-12 grid'>
                            {decadas.map((term) => (
                                <FooterItem
                                    key={term.slug}
                                    label={term.name}
                                    taxonomy={"decada"}
                                    termId={term.term_id}
                                />
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <div className='bg-red-300 text-white p-8'>
                <h2 className='text-xl lg:text-2xl font-semibold mb-4 md:px-12'>Temas</h2>
                <div className='md:px-12 md:columns-4'>
                    {temas.map((term) => (
                        <FooterItem
                            key={term.slug}
                            label={term.name}
                            taxonomy={"tema"}
                            termId={term.term_id}
                        />
                    ))}
                </div>
            </div>
            <div className='flex justify-evenly p-4 text-sm items-center bg-white'>
                <div>
                    <p>Realização: <a href='https://centrosabia.org.br/' target="_blank" rel='noreferrer'><img className="inline ml-3" width="80" height="100" src="https://centrosabia.org.br/wp-content/uploads/2024/10/sabia-cor-horizontal.jpg" alt="Centro Sabiá" /></a></p>
                </div>
                <div>
                    <p>Apoio: <a href='https://www.terredeshommesschweiz.ch/' target="_blank" rel='noreferrer'><img className="inline ml-3" width="150" height="50" src="https://centrosabia.org.br/wp-content/uploads/2021/10/TDH-1-300x81.png" alt="Terre des Hommes Schweiz" /></a></p>
                </div>
                <div>
                    <p>Desenvolvido pela <a href='https://eita.coop.br/' target="_blank" rel='noreferrer'>Cooperativa EITA</a></p>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
