import { classNames } from "@/libs/classNames";
import { NavbarTerm } from "@/types";
import { XMarkIcon } from "@heroicons/react/24/outline";
import { useRouter } from "next/router";
import React from "react";

interface NavbarFilterTitleProps {
  showBackground: boolean;
  terms: NavbarTerm[];
  title?: string;
  selectedFilter?: NavbarTerm;
  searchQuery?: string;
}

const NavbarFilterTitle: React.FC<NavbarFilterTitleProps> = ({
  showBackground,
  terms,
  title,
  selectedFilter,
  searchQuery,
}) => {
  const router = useRouter();
  if (!selectedFilter && !searchQuery) {
    return <></>;
  }
  return (
    <div
      className="cursor-pointer text-md md:text-xl font-semibold flex flex-row gap-3 md:p-2 items-center hover:bg-green-300 hover:bg-opacity-50 hover:rounded-md"
      title="Remover filtro"
      onClick={() => {
        if (terms.length === 1) {
          router.push("/");
          return;
        }

        if (searchQuery) {
          const remainingFilter = terms.filter(
            (term) => term.taxonomy !== "search"
          )[0];
          if (!remainingFilter) {
            router.push("/");
            return;
          }
          router.push(
            `/${remainingFilter.taxonomy}/?id=${
              remainingFilter.termId || remainingFilter.termSlug
            }`
          );
          return;
        }

        if (selectedFilter) {
          const remainingFilter = terms.filter(
            (term) => term.taxonomy !== selectedFilter.taxonomy
          )[0];

          if (!remainingFilter) {
            router.push("/");
            return;
          }
          router.push(
            `/${remainingFilter.taxonomy}/?${
              remainingFilter.taxonomy === "search" ? "s" : "id"
            }=${remainingFilter.termId || remainingFilter.termSlug}`
          );
          return;
        }

        router.push("/");
      }}
    >
      {title || (selectedFilter && selectedFilter.termName) || searchQuery}
      <div
        className={classNames(
          "h-4 w-4 rounded-full flex items-center justify-center",
          showBackground
            ? "bg-red-300 text-white"
            : "bg-white-50 bg-opacity-70 text-black"
        )}
      >
        <XMarkIcon className="w-4" />
      </div>
    </div>
  );
};

export default NavbarFilterTitle;
