import useSwr from "swr";
import fetcher from "@/libs/fetcher";
import { ItemInterface } from "@/types";

const useItem = (id?: number) => {
	const { data, error, isLoading } = useSwr(
		id ? `${process.env.NEXT_PUBLIC_WP_API_URL}item/${id}` : null,
		fetcher,
		{
			revalidateIfStale: false,
			revalidateOnFocus: false,
			revalidateOnReconnect: false,
		}
	);
	return {
		data,
		error,
		isLoading,
	} as {
		data: ItemInterface;
		error: any;
		isLoading: boolean;
	};
};

export const useItemBySlug = (path: string, slug?: string | string[]) => {
	const finalSlug = !slug ? undefined : Array.isArray(slug) ? slug[0] : slug;
	const { data, error, isLoading } = useSwr(
		slug ? `${process.env.NEXT_PUBLIC_WP_API_URL}${path}/${finalSlug}` : null,
		fetcher,
		{
			revalidateIfStale: false,
			revalidateOnFocus: false,
			revalidateOnReconnect: false,
		}
	);
	return {
		data,
		error,
		isLoading,
	} as {
		data: ItemInterface;
		error: any;
		isLoading: boolean;
	};
};

export default useItem;
