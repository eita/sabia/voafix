import { create } from "zustand";

export interface SubscriptionModalStoreInterface {
	isOpen: boolean;
	openModal: () => void;
	closeModal: () => void;
}

const useSubscriptionModalStore = create<SubscriptionModalStoreInterface>((set) => ({
	isOpen: false,
	openModal: () => set({ isOpen: true }),
	closeModal: () => set({ isOpen: false }),
}));

export default useSubscriptionModalStore;
