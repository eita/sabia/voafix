import { useState, useRef, useEffect } from 'react'

const useSliding = (elementWidthInRem: number, gapWidthInRem: number, countElements: number) => {
  // const defaultFontSize = parseFloat(getComputedStyle(document.documentElement).fontSize);
  const defaultFontSize = 16;
  const gapWidth = gapWidthInRem * defaultFontSize;
  const elementWidth = (elementWidthInRem) * defaultFontSize + (gapWidth / 2);
  const containerRef = useRef<HTMLDivElement>(null);
  const [containerWidth, setContainerWidth] = useState(0);
  const [distance, setDistance] = useState(0);
  const [totalInViewport, setTotalInViewport] = useState(0)
  const [viewed, setViewed] = useState(0);

  useEffect(() => {
    if (containerRef.current) {
      const containerWidth = containerRef.current.getBoundingClientRect().width;
      setContainerWidth(containerWidth);
      setTotalInViewport(Math.floor(containerWidth / elementWidth));
    }
  }, [containerRef, elementWidth]);

  const handlePrev = () => {
    setViewed(viewed - totalInViewport);
    // setDistance(distance + containerWidth);
    setDistance(distance + (totalInViewport * (elementWidth + (gapWidth / 2))));
  }

  const handleNext = () => {
    setViewed(viewed + totalInViewport);
    // setDistance(distance - containerWidth);
    setDistance(distance - (totalInViewport * (elementWidth + (gapWidth / 2))));
  }

  const slideProps = {
    style: { transform: `translate3d(${distance}px, 0, 0)` }
  };

  const hasPrev = distance < 0;
  const hasNext = (viewed + totalInViewport) <= countElements;

  return { handlePrev, handleNext, slideProps, containerRef, hasPrev, hasNext };
}

export default useSliding;