import useSwr from "swr";
import fetcher from "@/libs/fetcher";
import { Taxonomy, TaxonomyType, TaxonomyTypeExtended } from "@/types";

const useTaxonomyList = (taxonomy: TaxonomyTypeExtended, random?: boolean) => {
  let url: string | null = `${process.env.NEXT_PUBLIC_WP_API_URL}taxonomies/${taxonomy}`;
  if (random) {
    url += '?rand=1';
  }
  if (taxonomy === 'postType') {
    url = null;
  }
  const { data, error, isLoading } = useSwr(url, fetcher, {
	revalidateIfStale: false,
	revalidateOnFocus: false,
	revalidateOnReconnect: false,
  });
  if (taxonomy === 'postType') {
	return {
	  data: [{slug: 'audio', name: 'Podcast'}, {slug: 'video', name: 'Vídeo'}, {slug: 'wpdmpro', name: 'Documento PDF'}],
	  error: null,
	  isLoading: false
	} as {
	  data: Taxonomy[];
      error: any;
      isLoading: boolean;
	}
  }

  const array:Taxonomy[] = data && data[taxonomy] ? Object.values(data[taxonomy]) : [];
  return {
	data: array.map((item) => { return {term_id: item.term_id, slug: item.slug, name: item.name}}),
	error,
	isLoading,
  } as {
    data: Taxonomy[];
    error: any;
    isLoading: boolean;
  };
};

export default useTaxonomyList;
