import useSWRInfinite from 'swr/infinite'
import fetcher from "@/libs/fetcher";
import { ItemInterface, ItemType, RowsConfig } from "@/types";
import { getKey } from "@/libs/getKey";

const useItemList = (row: RowsConfig, postType?: ItemType) => {
	let url:string | null = `${process.env.NEXT_PUBLIC_WP_API_URL}items`;
	if (row.search) {
		url = `${process.env.NEXT_PUBLIC_WP_API_URL}items?s=${row.search}`;
		if (postType) {
			url += `&post_type=${postType}`
		}
	}
	if (row.taxonomy && row.term) {
		const taxonomyInUrl = row.taxonomy === 'postType' ? 'post_type' : row.taxonomy as string;
		const postTypeInUrl = postType ? `&post_type=${postType}` : '';
		url = `${process.env.NEXT_PUBLIC_WP_API_URL}items?${taxonomyInUrl}=${row.term}${postTypeInUrl}`;
	}
	if (!row.title) {
		url = null;
	}
	const { data, error, mutate, size, setSize, isValidating } = useSWRInfinite(
		(...args) => getKey(...args, url),
		fetcher
	)
	return {
		data,
		error,
		isValidating,
		size,
		setSize,
	} as {
		data: ItemInterface[][] | null;
		error: any;
		isValidating: boolean;
		size: number;
		setSize: (x: number | ((_size: number) => number)) => void;
	};
};

export default useItemList;
