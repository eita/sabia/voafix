import useSwr from "swr";
import fetcher from "@/libs/fetcher";
import { ItemInterface, ItemType, TaxonomyTypeExtended } from "@/types";

interface useBillboardProps {
	taxonomy?: TaxonomyTypeExtended;
	termSlug?: string;
	postType?: ItemType;
}

const useBillboard = ({ taxonomy, termSlug, postType }: useBillboardProps) => {
	let action: TaxonomyTypeExtended | 'post_type' | 's' | undefined;
	switch (taxonomy) {
		case 'decada':
		case 'tema':
		case 'wpdmcategory':
			action = taxonomy;
			break;
		case 'postType':
			action = 'post_type';
			break;
		case 'search':
			action = 's';
	}
	let url = (action && termSlug)
		? `${process.env.NEXT_PUBLIC_WP_API_URL}random_item?${action}=${termSlug}`
		: `${process.env.NEXT_PUBLIC_WP_API_URL}items?tema=destaque-principal`;

	if (postType) {
		url += `&post_type=${postType}`;
	}

	const { data, error, isLoading } = useSwr(url, fetcher, {
		revalidateIfStale: false,
		revalidateOnFocus: false,
		revalidateOnReconnect: false,
	});
	return {
		data: !data || ((action && termSlug) || postType) ? data : data[0],
		error,
		isLoading,
	} as {
		data: ItemInterface;
		error: any;
		isLoading: boolean;
	};
};

export default useBillboard;
