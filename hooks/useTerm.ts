import useSwr from "swr";
import fetcher from "@/libs/fetcher";
import { Term } from "@/types";

const useTerm = (termId: number | null) => {
	const { data, error, isLoading } = useSwr(termId ? `${process.env.NEXT_PUBLIC_WP_API_URL}term/${termId}` : null, fetcher, {
		revalidateIfStale: false,
		revalidateOnFocus: false,
		revalidateOnReconnect: false,
	});
	return {
		data,
		error,
		isLoading,
	} as {
        data: Term | null;
        error: any;
        isLoading: boolean;
    };
};

export default useTerm;
