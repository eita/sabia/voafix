import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

const useNavigationHistory = () => {
  const router = useRouter();
  const [navHistory, setNavHistory] = useState<string[]>([]);

  useEffect(() => {
        setNavHistory((prevNavHistory) => {
            if (router.asPath !== prevNavHistory[prevNavHistory.length - 1]) {
                return [...prevNavHistory, router.asPath];
            }
            return prevNavHistory
        });
  }, [router]);

  const handleBackNavigation = () => {
    if (navHistory.length > 1) {
      router.back();
      setNavHistory(navHistory.slice(0, -1))
      return;
    }
    setNavHistory(['/']);
    router.replace("/");
  };
  return {navHistory, handleBackNavigation};
};

export default useNavigationHistory;
