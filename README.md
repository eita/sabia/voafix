# VoaFlix - o espaço multimedia Sabiá
Bem vinda! Bem vindo!

## Instalação

### Pré-requisitos

Confirme que tem **Node.js versão 14.x** instalado.

### Instalação de dependências
```shell
yarn install
```

### Configure Variáveis do sistema
Renomeie .env.example para .env.local e coloque os valores correspondentes

### Inicie o App
O ambiente de desenvolvimento é iniciado com:

```shell
yarn run dev
```